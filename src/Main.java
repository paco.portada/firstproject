import java.io.*;


public class Main {

    public static int getResultadoFichero(String nombreFichero) {
        int valor = 0;
        try {
            FileInputStream fis = new FileInputStream(nombreFichero);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String linea = br.readLine();
            valor = Integer.parseInt(linea);
        } catch (FileNotFoundException e) {
            System.out.println("No se pudo abrir " + nombreFichero);
        } catch (IOException e) {
            System.out.println("No hay nada en " + nombreFichero);
        } finally {
            return valor;
        }
    }

    public static long getSumaTotal(String[] vocales) {
        long suma = 0;

        for (int i = 0 ; i < vocales.length; i++) {
            suma += getResultadoFichero(vocales[i] + ".txt");
        }
        return suma;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        long sumaTotal;
        String ficheroEntrada;
        ficheroEntrada = args[0];
        String[] vocales = {"a", "e", "i", "o", "u", "á", "é", "í", "ó", "ú"};
        File carpeta = new File("./");
        ProcessBuilder pb;
        String ficheroErrores;

        for (int i = 0; i < vocales.length; i++) {
            pb = new ProcessBuilder("java", "src/ProcesadorFichero.java", ficheroEntrada, vocales[i], vocales[i] + ".txt");
            //pb = new ProcessBuilder("java", "ProcesadorFichero", ficheroEntrada, vocales[i], vocales[i] + ".txt");
            pb.directory(carpeta);
            ficheroErrores = "Errores" + vocales[i] + ".txt";
            pb.redirectError(new File(ficheroErrores));
            pb.start();
        }

        //waitFor()

        Thread.sleep(2000);
        sumaTotal = getSumaTotal(vocales);
        System.out.println("La suma total de vocales es: " + sumaTotal);
    }


}
